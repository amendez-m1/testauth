package com.example.spring;

import com.example.spring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.Optional;

@Service
public class TokenService {

    HashMap<String, String> tokens = new HashMap<>(); //<user, token>

    @Transactional
    public String findToken(User user){
        return tokens.get(user.getName());
    }

    @Transactional
    public String addToken(User user, String token){
        return tokens.put(user.getName(), token);
    }
}
