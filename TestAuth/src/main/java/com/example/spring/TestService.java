package com.example.spring;

import com.example.spring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@EnableJpaRepositories(basePackages = "com.example.spring.repository")
public class TestService {

    @Autowired
    UserRepository userRepository;

    @Transactional
    public void addUser(User user){
        userRepository.save(user);
    }

    @Transactional
    public Optional<User> findByName(String name){
        return userRepository.findById(name);
    }
}
