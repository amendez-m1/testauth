package com.example.spring.controller;

import com.example.spring.TestService;
import com.example.spring.TokenService;
import com.example.spring.User;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.HashMap;

@RestController
@RequestMapping("/api")
public class RestControl {

    @Autowired
    private TestService service;

    @Autowired
    private TokenService tokenService;


    @GetMapping("/login")
    public ResponseEntity<User> apiUser(@RequestBody User user){

        var u = service.findByName(user.getName());
        if(u.isEmpty()){
            return ResponseEntity.badRequest().body(new User("user not", "found"));
        }
        var token = tokenService.findToken(user);
        if(token == null){
            return ResponseEntity.badRequest().body(new User("token not", "found"));
        }

        HttpHeaders head = new HttpHeaders();
        head.set("token", token);
        return ResponseEntity.ok().headers(head).body(user);
    }

    @PostMapping("/signup")
    public ResponseEntity<User> signup(@RequestBody @NotNull User user){
        service.addUser(user);

        var token = RandomStringUtils.random(32, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@#");
        tokenService.addToken(user, token);

        HttpHeaders head = new HttpHeaders();
        head.set("token", token);
        return ResponseEntity.ok().headers(head).body(user);
    }

    @PostMapping("/test")
    public Object apiPost(@RequestBody User user, @RequestHeader HashMap<String, String> headers ) {
        System.out.println(user.getName() + "\n" + headers.getOrDefault("token", "nope"));
        if(tokenService.findToken(user).equals(headers.getOrDefault("token", "")))
            return user;
        return new User("not the", "good token");
    }

}
