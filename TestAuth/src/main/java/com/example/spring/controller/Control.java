package com.example.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class Control {

    @GetMapping("/")
    public String home(){
        return ("welcome");
    }

    @GetMapping("/user")
    public String user(){
        return ("welcome");
    }

    @GetMapping("/admin")
    public String admin(){
        return ("welcome");
    }

}
